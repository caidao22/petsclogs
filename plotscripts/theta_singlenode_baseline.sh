#!/usr/bin/bash

petscplot_dir=../../petscplot/
python3 ${petscplot_dir}/petscplot.py -t flop --event MatMult  \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy1024_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy1024_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy1024_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy2048_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy2048_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy2048_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy4096_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy4096_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy4096_n{16,32,64}.log : \
--legend-labels='1024$\times$1024, flat, MCDRAM : 1024$\times$1024, flat, DRAM : 1024$\times$1024, cache:2048$\times$2048, flat, MCDRAM : 2048$\times$2048, flat, DRAM : 2048$\times$2048, cache : 4096$\times$4096, flat, MCDRAM : 4096$\times$4096, flat, DRAM : 4096$\times$4096, cache' \
--title='Baseline SpMV Performance for various grid sizes' \
-m paper --output=sc_avx512_theta_singlenode_baseline_bar.eps

