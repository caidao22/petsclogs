#!/usr/bin/bash

psize=2048

petscplot_dir=../petscplot/
${petscplot_dir}/petscplot -t flop --event MatMult  \
logs_theta_sn/ex5adj_avx512_theta_flat_mg_sell_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_avx2_theta_flat_mg_sell_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_avx_theta_flat_mg_sell_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_avx2_theta_flat_mg_aij_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_avx_theta_flat_mg_aij_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_avx512_theta_flat_mg_aijperm_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_baseline_theta_flat_mg_aij_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
logs_theta_sn/ex5adj_avx512_theta_flat_mg_aijmkl_fwdonly_xy${psize}_n{4,8,16,32,64}.log : \
--legend-labels='SELL using AVX512:SELL using AVX2:SELL using AVX:CSR using AVX512:CSR using AVX2: CSR using AVX: CSRPerm:CSR baseline:MKL CSR' --title='SpMV Performance ('${psize}'$\times$'${psize}" grid,about 8 million DOF)" \
--legend-outside=True --width-pt=480 \
-m paper --output=sc_theta_sn_xy${psize}.eps

#./petscplot -t flop --event MatMult  \
#logs_theta_sn/ex5adj_theta_flat_mg_sell_fwdonly_xy1024_n{4,8,16,32,64}.log  \
#--legend-labels='SELL-avx512:SELL' --title='SpMV Performance ('"1024"'$\times$'"1024 grid,about 2 million DOF)" \
#-m paper --output=sc_theta_xy1024.pdf

#./petscplot -t flop --event MatMult  \
#logs_knl/ex5adj_knl_none_aij_opt_fwdonly_xy1024_n{2,4,8,16,32,64}.log : \
#logs_knl/ex5adj_knl_none_aij_novecrem_fwdonly_xy1024_n{2,4,8,16,32,64}.log : \
#logs_knl/ex5adj_knl_none_aij_novec_fwdonly_xy1024_n{2,4,8,16,32,64}.log \
#--legend-labels='AIJ(avx512):AIJ(avx512 no-vec-remainder):AIJ(no-vec)' --title='SpMV Performance ('"1024"'$\times$'"1024 grid,about 2 million DOF)" \
#-m paper --output=sc_memalign_knl_aij_opts_xy1024.pdf
