#!/usr/bin/bash

petscplot_dir=../../petscplot/
python3 ${petscplot_dir}/singlexeon.py --event MatMult  \
../logs_haswell/ex5adj_avx_haswell_mg_aijmkl_fwdonly_xy2048_n18.log ../logs_haswell/ex5adj_{novec,avx,avx2}_haswell_mg_{aij,sell}_fwdonly_xy2048_n18.log : \
../logs_broadwell/ex5adj_avx_broadwell_mg_aijmkl_fwdonly_xy2048_n22.log ../logs_broadwell/ex5adj_{novec,avx,avx2}_broadwell_mg_{aij,sell}_fwdonly_xy2048_n22.log : \
../logs_skylake/ex5adj_avx_skylake_mg_aijmkl_fwdonly_xy2048_n28.log ../logs_skylake/ex5adj_{novec,avx,avx2,avx512}_skylake_mg_{aij,sell}_fwdonly_xy2048_n28.log : \
../logs_theta_sn/ex5adj_avx512_theta_flat_mg_aijmkl_fwdonly_xy2048_n64.log ../logs_theta_sn/ex5adj_{novec,avx,avx2,avx512}_theta_flat_mg_{aij,sell}_fwdonly_xy2048_n64.log : \
--legend-labels='MKL:CSR using novec:SELL using novec:CSR using AVX:SELL using AVX:CSR using AVX2:SELL using AVX2:CSR using AVX512:SELL using AVX512' \
--series-labels='Haswell:Broadwell:Skylake:KNL' \
--title='SpMV Performance on Xeon' \
-m paper --output=sc_xeon_singlenode.eps
#../logs_theta_mn/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
#../logs_theta_mn/ex5adj_avx512_theta_flat_dramonly_mg_sell_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \

