#!/usr/bin/bash

petscplot_dir=../../petscplot/
python3 ${petscplot_dir}/singleknl.py --event MatMult  \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy1024_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy2048_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy4096_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy1024_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy2048_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy4096_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy1024_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy2048_n{16,32,64}.log : \
../logs_theta_sn2_baseline/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy4096_n{16,32,64}.log : \
--legend-outside true \
--legend-labels='1024$\times$1024 grid : 2048$\times$2048 grid : 4096$\times$4096 grid' \
-m paper_bar --output=sc_avx512_theta_singlenode_baseline_bar.pdf
