#!/usr/bin/bash

python3 ../tools/multiknl.py --event MatMult  \
../logs_theta_flat/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
../logs_theta_flat/ex5adj_avx512_theta_flat_mg_sell_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
../logs_theta_cache/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
../logs_theta_cache/ex5adj_avx512_theta_cache_mg_sell_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
--legend-labels='flat mode using DRAM only:flat mode:cache mode' \
--title='SpMV Performance on Theta' \
-m paper --output=sc_avx512_theta_multinode.eps
#../logs_theta_mn/ex5adj_avx512_theta_flat_dramonly_mg_aij_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
#../logs_theta_mn/ex5adj_avx512_theta_flat_dramonly_mg_sell_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
#../logs_theta_mn_mglv7/ex5adj_avx512_theta_flat_mg_aij_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
#../logs_theta_mn_mglv7/ex5adj_avx512_theta_flat_mg_sell_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
#../logs_theta_mn_mglv7/ex5adj_avx512_theta_cache_mg_aij_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \
#../logs_theta_mn_mglv7/ex5adj_avx512_theta_cache_mg_sell_fwdonly_xy16384_n{4096,8192,16384,32768}.log : \

