#!/usr/bin/env python3
# -*- mode: Python -*-
from petscplot import *
rcParams['hatch.color'] = 'white'
import seaborn as sns

def plot_singlexeon(opts, logfiles):
  '''Plots strong scalability given a list of log files.  The file
  sequence should have a constant global problem size with increasing
  numbers of processes The result is a bar plot showing FLOPs
  on different architectures.'''
  ax = axes()
  marker = itertools.cycle(list('ov^<>*xd+H'))
  (plotstage,) = opts.stages
  plotevents = opts.events
  name = name_segments(opts,logfiles)
  #colors = ['r', 'b', 'm', 'c', 'y', '#624ea7', 'g', 'yellow', 'k', 'maroon']
  colors = sns.color_palette("Paired", 10)
  colors.insert(0,"#34495e")
  fcolors = ['#228b22','#8a2121','#21218a']
  width = 1
  eventgap = 1
  seriesgap = 2
  alls = array([]) # end of each series
  ally = array([])
  alls = append(alls,0)
  shift = 0
  avxs = ['avx','avx2','avx512']
  mats = ['sell','aij','aijmkl']
  xeons = opts.series_labels

  for i,logs in enumerate(segment(logfiles)):
    series = [parse(tokenize(read_file(fname))) for fname in logs]
    index = arange(len(series))
    bottom = zeros(len(index))
    shift = alls[-1]
    for j,event in enumerate(plotevents):
      flop = array([s.stages[plotstage].events[event].Mflops/1024 for s in series])
      ally = concatenate((ally,flop))
      if j>1 :
        shift += eventgap
      if i == 3 : # add labels
        for k in index :
          bar = ax.bar(index[k]+shift,flop[k],width,color=colors[k],label=next(name),linewidth=0,bottom=bottom,zorder=3)
      else :
        bar = ax.bar(index+shift,flop,width,color=colors,linewidth=0,bottom=bottom,zorder=3)
      shift += len(series)
    alls = append(alls,shift+seriesgap) # center of each series
  alls = array([(y+x)/2-1 for x, y in zip(alls,alls[1:])])
  ax.grid(which='major', axis='y', linestyle='--',zorder=0)
  ax.set_ylabel('Performance [\\texttt{Gflop/s}]')
  ax.tick_params(axis='x',length=0,labelsize=12)
  ax.xaxis.set_major_locator(FixedLocator(alls))
  ax.xaxis.set_major_formatter(FixedFormatter(xeons))
  ax.legend(loc=(opts.legend_loc if opts.legend_loc else 'upper left'), frameon=False)
  ax.set_ylim(0, max(ally)*1.5)

if __name__ == "__main__":
  opts = parse_options()
  plot_singlexeon(opts,opts.logfiles)
  if opts.output:
    savefig(opts.output,bbox_inches="tight")
  else:
    show()
