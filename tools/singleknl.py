#!/usr/bin/env python3
# -*- mode: Python -*-
from petscplot import *
import seaborn as sns
rcParams['hatch.color'] = 'white'

def plot_singleknl(opts, logfiles):
    '''Plots strong scalability given a list of log files.  The file
    sequence should have a constant global problem size with increasing
    numbers of processes. The result is a bar plot showing FLOPs verus
    number of processes.'''
    fig, (ax1, ax2, ax3) = subplots(3)
    marker = itertools.cycle(list('ov^<>*xd+H'))
    (plotstage,) = opts.stages
    plotevents = opts.events
    name = name_segments(opts,logfiles)
    barwidth = 0.1
    seriesgap = 0.15
    colors = sns.color_palette("Set2", 10)
    hatches = ['///','\\\\\\','xxx']
    ax = ax1
    ally = array([])
    alls = array([]) # end of each series
    shift = 0
    for i,logs in enumerate(segment(logfiles)):
        if i == 3:
          ax = ax2
          ally = array([])
          shift = 0
        elif i == 6:
          ax = ax3
          ally = array([])
          shift = 0
        series = [parse(tokenize(read_file(fname))) for fname in logs]
        series.sort(key=lambda a : a.np) # Sort files in increasing order of number of processes
        #solves = [s.solves[-1] for s in series]
        np = array([s.np for s in series])
        index = arange(len(np))
        ht = hatches[i%len(np)]
        cl = colors[i%len(np)]
        #lb = next(name) if i%2 == 0 else None
        bottom = zeros(len(np))
        for j,event in enumerate(plotevents):
            flop = array([s.stages[plotstage].events[event].Mflops/1024 for s in series])
            ally = concatenate((ally,flop))
            onetimelb = next(name) if j==0 and i<3 else None
            bar = ax.bar(index+shift,flop,barwidth,color=cl,linewidth=0,hatch=ht,label=onetimelb,bottom=bottom)
            bottom += flop
        shift += seriesgap
        if (i+1)%3 == 0:
            ax.grid(which='major', axis='y', linestyle='--',zorder=0)
            ax.tick_params(axis='x',which='minor',bottom=False) # turn off minor ticks on x axis
            ax.xaxis.set_ticks(index+seriesgap)
            ax.xaxis.set_ticklabels([])
            ax.xaxis.set_minor_locator(ticker.NullLocator())
            ax.yaxis.set_ticks([5,10,20,30,40,50])
            ax.yaxis.set_ticklabels([5,10,20,30,40,50])
            ax.yaxis.set_minor_formatter(ticker.NullFormatter())
            ax.set_ylim(0,28)
    ax1.set_title('flat mode, MCDRAM',position=(0.5, 0.6))
    ax2.set_title('flat mode, DRAM',position=(0.5, 0.6))
    ax3.set_title('cache mode',position=(0.5, 0.6))
    ax3.set_xlabel('Number of processes')
    ax3.xaxis.set_ticklabels(np)
    ax2.set_ylabel('Performance [\\texttt{Gflop/s}]')
    if opts.legend_outside :
      ax1.legend(numpoints=1,bbox_to_anchor=(0,1.02,1,0.2),loc=(opts.legend_loc if opts.legend_loc else 'lower left'),mode="expand", borderaxespad=0, ncol=3, frameon=False)
    else :
      ax1.legend(numpoints=1,loc=(opts.legend_loc if opts.legend_loc else 'upper left'), frameon=False)

if __name__ == "__main__":
  opts = parse_options()
  plot_singleknl(opts,opts.logfiles)
  if opts.output:
    savefig(opts.output,bbox_inches="tight")
  else:
    show()
