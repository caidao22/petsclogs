#!/usr/bin/env python3
# -*- mode: Python -*-
from petscplot import *
rcParams['hatch.color'] = 'white'

def plot_multiknl(opts, logfiles):
    '''Plots strong scalability given a list of log files.  The file
    sequence should have a constant global problem size with increasing
    numbers of processes The result is a bar plot showing time to
    solve the problem with successively more nodes.'''
    ax = axes()
    marker = itertools.cycle(list('ov^<>*xd+H'))
    (plotstage,) = opts.stages
    plotevents = opts.events
    name = name_segments(opts,logfiles)
    colors = 'rgbmcyk'
    fcolors = ['#228b22','#8a2121','#21218a']
    width = 0.08
    allx = array([])
    ally = array([])
    shift = 0
    for i,logs in enumerate(segment(logfiles)):
        series = [parse(tokenize(read_file(fname))) for fname in logs]
        series.sort(key=lambda a : a.np) # Sort files in increasing order of number of processes
        #solves = [s.solves[-1] for s in series]
        np = array([s.np for s in series])
        index = arange(len(np))
        #ttime = array([r.wtime for r in series]) # total tim
        ttime = array([r.stages[plotstage].events['TSStep'].time for r in series]) # total time for TSStep excluding initialization time
        #ideal_np,ideal_flop = fit_ideal_loglog(np,flop,slope=1)
          #loglog(ideal_np+shift,ideal_flop,'--',color='gray',label='ideal')
        allx, ally = concatenate((allx,index+i*0.1)), concatenate((ally,ttime))
        lastline = None
        ht = None if i%2 == 0 else 'xxx'
        lb = next(name) if i%2 == 0 else None
        ax.bar(index+i*0.1,ttime,width,facecolor=fcolors[i//2],linewidth=0,hatch=ht,label=lb)
        bottom = zeros(len(np))
        for j,event in enumerate(plotevents):
          etime = array([r.stages[plotstage].events[event].time for r in series]) # event time
          allx = concatenate((allx,np))
          ally = concatenate((ally,etime))
          onetimelb = event if i==2 else None
          bar = ax.bar(index+i*0.1,etime,width,facecolor='#ffd700',linewidth=0,label=onetimelb,bottom=bottom)
          bottom += etime
    ax.set_xlabel('Number of KNL Nodes',fontsize=16)
    ax.set_ylabel('Wall time (sec)',fontsize=16)
    # ax.tick_params(axis='x',which='minor',bottom=False) # turn off minor ticks on x axis
    ax.xaxis.set_ticks(index+i*0.1/2)
    ax.xaxis.set_ticklabels(np/64)
    # ax.yaxis.set_ticks([1,2,5,10,20,30,40])
    # ax.yaxis.set_ticklabels([1,2,5,10,20,30,40])
    # ax.set_ylim(min(ally)*0.8, max(ally)*1.6)
    # ax.xaxis.set_minor_locator(ticker.NullLocator())
    # ax.yaxis.set_minor_formatter(ticker.NullFormatter())
    ax.legend(loc=(opts.legend_loc if opts.legend_loc else 'upper right'), frameon=False)
    if opts.title is None:
        ax.set_title('Strong scaling')
    elif opts.title:
        ax.set_title(opts.title)

if __name__ == "__main__":
    opts = parse_options()
    plot_multiknl(opts,opts.logfiles)
    if opts.output:
        savefig(opts.output,bbox_inches="tight")
    else:
        show()
